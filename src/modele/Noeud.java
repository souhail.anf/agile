package modele;
import java.util.ArrayList;


public class Noeud {

	private Long id;
	private double latitude;
	private double longitude;
	private ArrayList<Troncon> tronconsPartants;

	public Noeud() {
		this.tronconsPartants = new ArrayList<Troncon>();
	}

	public Noeud(Long id, double latitude, double longitude, ArrayList<Troncon> tronconsPartants) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		if (tronconsPartants!=null)
			this.tronconsPartants = tronconsPartants;
		else
			this.tronconsPartants = new ArrayList<Troncon>();
	}

	public int calculDistanceEuclidienne(Noeud arrivee) {
		//TO DO
		return 0;
	}

	public void addTronconPartant(Troncon t) {
		this.tronconsPartants.add(t);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public ArrayList<Troncon> getTronconsPartants() {
		return tronconsPartants;
	}
	public void setTronconsPartants(ArrayList<Troncon> tronconsPartants) {
		this.tronconsPartants = tronconsPartants;
	}

	public String toString(){
		String res = "Noeud :";
		res+= ", id = "+ this.id;
		res+= ", latitude= " + this.latitude;
		res+= ", longitude= " + this.longitude;
		return res;
	}



}
