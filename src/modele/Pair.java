package modele;

public class Pair<F, S> {
	private F first;
	private S second;
	
	public Pair (F o1, S o2) {
		first = o1;
		second = o2;
	}
	
	public F getFirst() {
		return first;
	}
	
	public S getSecond() {
		return second;
	}
}
