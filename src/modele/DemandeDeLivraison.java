package modele;
import java.util.ArrayList;
import java.util.Observable;
public class DemandeDeLivraison extends Observable{
	
	private Entrepot entrepot;
	private ArrayList<PointDeLivraison> pointsDeLivraison;
	private int nombreLivreurs;
	
	
	public DemandeDeLivraison(Entrepot unEntrepot,ArrayList<PointDeLivraison> desPointsDeLivraison, int unNombreDeLivreurs) {
		this.entrepot = unEntrepot;
		this.pointsDeLivraison = desPointsDeLivraison;
		this.nombreLivreurs = unNombreDeLivreurs;
	}
	public DemandeDeLivraison(Entrepot unEntrepot,ArrayList<PointDeLivraison> desPointsDeLivraison) {
		this.entrepot = unEntrepot;
		this.pointsDeLivraison = desPointsDeLivraison;
		this.nombreLivreurs = 1;
	}
	
	public DemandeDeLivraison(){
		this.nombreLivreurs = 1;
		this.entrepot = null;
		this.pointsDeLivraison = new ArrayList<PointDeLivraison>();		
	}
	
	public Entrepot getEntrepot() {
		return entrepot;
	}
	public void setEntrepot(Entrepot entrepot) {
		this.entrepot = entrepot;
		setChanged();
		notifyObservers();			
	}
	public ArrayList<PointDeLivraison> getPointsDeLivraison() {
		return pointsDeLivraison;
	}
	public void setPointsDeLivraison(ArrayList<PointDeLivraison> pointsDeLivraison) {
		this.pointsDeLivraison = pointsDeLivraison;
		setChanged();
		notifyObservers();		
	}
	public int getNombreLivreurs() {
		return nombreLivreurs;
	}
	public void setNombreLivreurs(int nombreLivreurs) {
		this.nombreLivreurs = nombreLivreurs;
		setChanged();
		notifyObservers();	
	}
	
	public void calculerEnsembleLivraisons() {
		// TODO Auto-generated method stub
	}
	
	public String toString(){
		String res= "";
		res += "Nombre de Livreurs = " + this.nombreLivreurs;
		res += this.entrepot.toString();
		res += this.pointsDeLivraison.toString();
		return res;
	}
	
}
