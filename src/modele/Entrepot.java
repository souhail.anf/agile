package modele;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Entrepot extends Noeud {

	private Date heureDepart;
	
	public Date getHeureDepart() {
		return heureDepart;
	}

	public Entrepot(Long id, double latitude, double longitude, ArrayList<Troncon> tronconsPartants, Date heureDepart) {
		super(id, latitude, longitude, tronconsPartants);
		this.heureDepart = heureDepart;
		
	}
	
	public Entrepot(Noeud noeud, Date heureDepart) {
		super(noeud.getId(),noeud.getLatitude(),noeud.getLongitude(),noeud.getTronconsPartants());
		this.heureDepart = heureDepart;
	}
	
	public String toString()
	{
		String res = "Entrepot :";
		res+= "Id = "+ this.getId();
		res+= ", Latitude = " + this.getLatitude();
		res+= ", Longitude = " + this.getLongitude();
		
		SimpleDateFormat monFormat = new SimpleDateFormat("hh:mm:ss");		
		res+= ", HeureDeDepart = " + monFormat.format(heureDepart) ;
		
		return res;		
	}
	
	
	
}
