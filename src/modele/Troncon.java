package modele;
public class Troncon {

	private double longueur;
	private String nom;
	private Noeud noeudOrigine;
	private Noeud noeudDestination;
	
	public Troncon() {}
	public Troncon(double uneLongueur, String unNom, Noeud uneOrigine, Noeud uneDestination) {
		
		this.longueur = uneLongueur;
		this.nom = unNom;
		this.noeudOrigine = uneOrigine;
		this.noeudDestination = uneDestination;
		
	}
	public double getLongueur() {
		return longueur;
	}
	
	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Noeud getNoeudOrigine() {
		return noeudOrigine;
	}
	
	public void setNoeudOrigine(Noeud noeudOrigine) {
		this.noeudOrigine = noeudOrigine;
	}
	public Noeud getNoeudDestination() {
		return noeudDestination;
	}
	
	public void setNoeudDestination(Noeud noeudDestination) {
		this.noeudDestination = noeudDestination;
	}
	
	public String toString(){
		String res = "Troncon{"+ this.noeudOrigine.getId()+"->"+ this.noeudDestination.getId()+" "
				+" Nom = "+ this.nom+", Longueur= " + this.longueur + "}";
		return res;
	}
}
