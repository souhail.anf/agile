package modele;

import java.util.ArrayList;

public class Trajet {
	final private Noeud noeudDepart;
	final private Noeud noeudArrivee;
	final private double longueur;
	final private ArrayList<Troncon> parcoursTroncons;
	
	public Trajet(ArrayList<Troncon> parcoursTroncons){
		this.noeudDepart = parcoursTroncons.get(0).getNoeudOrigine();
		this.noeudArrivee = parcoursTroncons.get(parcoursTroncons.size()-1).getNoeudDestination(); 	
		this.parcoursTroncons = parcoursTroncons;
		double longueurTemp = 0;
		for(Troncon t : parcoursTroncons) {
			longueurTemp += t.getLongueur();
		}
		this.longueur = longueurTemp;
	}

	public Noeud getNoeudDepart() {
		return noeudDepart;
	}

	public Noeud getNoeudArrivee() {
		return noeudArrivee;
	}

	public ArrayList<Troncon> getParcoursTroncons() {
		return parcoursTroncons;
	}

	public double getLongueur() {
		return longueur;
	}
	
	public String toString() {
		 
		String renvoi = "Trajet{" + noeudDepart.getId() + "->" + noeudArrivee.getId() + " : " + longueur + " par " + parcoursTroncons.size() + " troncons[";
		for(Troncon t : parcoursTroncons)
			renvoi += t.toString();
		return renvoi;
	}
}