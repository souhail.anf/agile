package modele;

import java.util.ArrayList;
import java.util.Collection;

public class Graphe {
	private DemandeDeLivraison demandeDeLivraison;
	private Trajet[][] matriceTrajet;
	private Plan plan;
	
	public Graphe(Plan plan, DemandeDeLivraison demandeDeLivraison){
		this.demandeDeLivraison = demandeDeLivraison;
		this.plan = plan;
		ArrayList<Noeud> noeuds = new ArrayList<Noeud>(this.demandeDeLivraison.getPointsDeLivraison());
		noeuds.add(this.demandeDeLivraison.getEntrepot());
		this.matriceTrajet = new Trajet[noeuds.size()][noeuds.size()];
		for(int i=0; i<noeuds.size(); i++){
			Noeud noeudCourant = noeuds.remove(i);
			Collection<Trajet> plusCourtsChemins = this.plan.calculerPlusCourtChemin(noeudCourant, noeuds);
			for(Trajet t : plusCourtsChemins){
				int indexDepart = i;
				Long idNoeudArrivee = t.getNoeudArrivee().getId();
				Noeud noeudArrivee = null;
				for(Noeud n : noeuds) {
					if(n.getId()==idNoeudArrivee) noeudArrivee = n;
				}
				int indexArrivee = noeuds.indexOf(noeudArrivee);
				matriceTrajet[indexDepart][indexArrivee] = t;
			}
			noeuds.add(i,noeudCourant);
		}
	}

	private int getIndexOf(Long id) {
		if(id==this.demandeDeLivraison.getEntrepot().getId())
			return this.demandeDeLivraison.getPointsDeLivraison().size();
		
		ArrayList<Noeud> noeuds = new ArrayList<Noeud>(this.demandeDeLivraison.getPointsDeLivraison());
		for(Noeud n : noeuds) {
			if(n.getId()==id) return getIndexOf(n);
		}
		return -1;
	}
	
	private int getIndexOf(Noeud noeud) {
		int index = this.demandeDeLivraison.getPointsDeLivraison().indexOf(noeud);
		if(index<0 && noeud.getId()==this.demandeDeLivraison.getEntrepot().getId())
			index = this.demandeDeLivraison.getPointsDeLivraison().size();
		if(index<0) {
			try {
				Noeud noeudDuPlan = this.plan.rechercheNoeudParId(noeud.getId());
				return getIndexOf(noeudDuPlan.getId());
			}
			catch(Exception e){}
		}
		return index;
			
	}

	public Trajet getTrajet (Noeud noeudDepart, Noeud noeudArrivee){
		int indexDepart = getIndexOf(noeudDepart);
		int indexArrivee = getIndexOf(noeudArrivee);
		if(indexDepart<0 || indexArrivee<0)
			return null;
		return matriceTrajet[indexDepart][indexArrivee];
	}

	public DemandeDeLivraison getDemandeDeLivraison() {
		return demandeDeLivraison;
	}

	public Plan getPlan() {
		return plan;
	}
}