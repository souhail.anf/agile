package modele;

import java.util.ArrayList;
import java.util.Observable;

public class EnsembleDeTournees extends Observable{
	
	private ArrayList<Tournee> listeTournee ;
	private Entrepot entrepot;
	 
	 public EnsembleDeTournees(ArrayList<Tournee> listeTournee, Entrepot entrepot) {
		 this.setListeTournee(listeTournee);
		 this.setEntrepot(entrepot);
	 }
	 
	public EnsembleDeTournees(){
		 this.entrepot = null;
		 this.listeTournee = new ArrayList<Tournee>();
	 }

	public ArrayList<Tournee> getListeTournee() {
		return listeTournee;
	}

	public void setListeTournee(ArrayList<Tournee> listeTournee) {
		this.listeTournee = listeTournee;
		setChanged();
		notifyObservers();		
	}

	public Entrepot getEntrepot() {
		return entrepot;
	}

	public void setEntrepot(Entrepot entrepot) {
		this.entrepot = entrepot;
	}
	

}
