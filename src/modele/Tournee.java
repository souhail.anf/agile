package modele;

import java.util.ArrayList;

public class Tournee {
	final private double longueur;
	final private ArrayList<Pair<PointDeLivraison,Trajet>> listePointTrajet;
	final private int livreur;

	public Tournee (double longueur, int livreur, ArrayList<Pair<PointDeLivraison,Trajet>> listePointTrajet){
		this.longueur = longueur;
		this.livreur = livreur;
		this.listePointTrajet = listePointTrajet;
	}

	public double getLongueur() {
		return longueur;
	}

	public ArrayList<Pair<PointDeLivraison,Trajet>> getListePointTrajet() {
		return listePointTrajet;
	}

	public int getLivreur() {
		return livreur;
	}
}