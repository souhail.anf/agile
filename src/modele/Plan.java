package modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.PriorityQueue;

public class Plan extends Observable{

	private HashMap<Long,Noeud> noeuds;
	private ArrayList<Troncon> troncons;
	
	int largeurplan;
	int hauteurplan;
	double latmin;
	double latmax;
	double longmin;
	double longmax;
	
	public Plan() {}
	
	public Plan(int largeurplan, int hauteurplan) {
		this.largeurplan = largeurplan;
		this.hauteurplan = hauteurplan;
	}
	
	public int getLargeurplan() {
		return largeurplan;
	}

	public void setLargeurplan(int largeurplan) {
		this.largeurplan = largeurplan;
	}

	public int getHauteurplan() {
		return hauteurplan;
	}

	public void setHauteurplan(int hauteurplan) {
		this.hauteurplan = hauteurplan;
	}

	public double getLatmin() {
		return latmin;
	}

	public void setLatmin(double latmin) {
		this.latmin = latmin;
	}

	public double getLatmax() {
		return latmax;
	}

	public void setLatmax(double latmax) {
		this.latmax = latmax;
	}

	public double getLongmin() {
		return longmin;
	}

	public void setLongmin(double longmin) {
		this.longmin = longmin;
	}

	public double getLongmax() {
		return longmax;
	}

	public void setLongmax(double longmax) {
		this.longmax = longmax;
	}

	public Noeud rechercheNoeudParId(Long unId) throws Exception {
		return noeuds.get(unId);
	}

public int comparateurDeDistances(double a, double b) {
		
		if(a<b) {
			
			return -1;
		}else if(a>b) {
			
			return 1;	
		}
		return 0;
		
	}
	
	public Collection<Trajet> calculerPlusCourtChemin(Noeud noeudDepart, ArrayList<Noeud> noeudsArrivee){ //on ne passe pas la collection d'arrivee pour faciliter l'approche pr�alable
		
		Collection<Trajet> ensembleDijsktraaNoeuds = new ArrayList<Trajet>(); //Valeur de retour
		int nombreSommets = noeuds.size();		// nombre de sommets
		boolean sommetsNoirs[]  = new boolean[nombreSommets]; //Noeuds de 0 � n-1 suivant l'indice
		Arrays.fill(sommetsNoirs, false);
		double distance[] = new double[nombreSommets];		//tableau d
		Arrays.fill(distance, Double.MAX_VALUE);		//on remplit le tableau distance d'infini	
		int predecesseur[] = new int[nombreSommets];	
		Arrays.fill(predecesseur,nombreSommets+1); //on remplit les predecesseurs d'un int qui ne reference pas un sommet
		HashMap<Integer,ArrayList<Troncon>> troncon = new HashMap<Integer,ArrayList<Troncon>>();
		HashMap<Long, Integer> tableDeCorrespondanceIdSommet = new HashMap<Long,Integer>();  // on connait l'id on veut le sommet
		HashMap<Integer, Long> tableDeCorrespondanceSommetId = new HashMap<Integer,Long>();		//on connait le sommet on veut l'id
		PriorityQueue<Integer> fileDePriorite = new PriorityQueue<Integer>(nombreSommets, (a,b) ->  comparateurDeDistances(distance[a],distance[b])); //priority queue qui trie les valeurs en fonction des distances dans le tableau 
		int correspondance = 0;
		for (Map.Entry<Long, Noeud> entry : noeuds.entrySet()) {
			Long idTempo = entry.getKey();							//on construit les tableaux de correspondance
			Integer sommetTempo = new Integer(correspondance);
			tableDeCorrespondanceIdSommet.put(idTempo,sommetTempo);
			tableDeCorrespondanceSommetId.put(sommetTempo,idTempo);
			correspondance++;
			
		}
		Long idNoeudDepart = noeudDepart.getId();
		fileDePriorite.add(tableDeCorrespondanceIdSommet.get(idNoeudDepart));
		distance[tableDeCorrespondanceIdSommet.get(idNoeudDepart)] = 0;
		troncon.put(tableDeCorrespondanceIdSommet.get(idNoeudDepart), new ArrayList<Troncon>());
		//in fine, on obtient un table de hashage avec pour cl� l'id du Noeud et pour valeur l'indice dans le graphe
		
		while(fileDePriorite.size()!=0) {
			
			Integer sommetCourant = fileDePriorite.poll(); //on extrait le sommet le plus proche grace � la file de prio
			double distanceNoeud = distance[sommetCourant];	//on recupere la distance de ce noeud
			
			if(!sommetsNoirs[sommetCourant]) {			//si le noeud est noir on passe
				sommetsNoirs[sommetCourant] = true;		//on d�finit le noeud courant comme �tant noir = termin� � la fin de l'it�ration
				
				//possibilit� de rajouter un break ici si une cible est atteinte (if sommetCourant == cible break)
				
				
				try {
					
					for(Troncon t : rechercheNoeudParId(tableDeCorrespondanceSommetId.get(sommetCourant)).getTronconsPartants()) {
						//on parcourt les troncons du noeud courant pour r�cup�rer les noeuds destinations et les longueurs
						Noeud voisin = t.getNoeudDestination();				//on r�cup�re destination
						Integer numeroSommetVoisin = tableDeCorrespondanceIdSommet.get(voisin.getId()); //on recupere son num�ro de sommet
						double distanceVoisin = distanceNoeud + t.getLongueur();   //on calcule la distance (longueur du troncon + distance du noeud courant (qui est �galement le noeud origine)
						if(distanceVoisin<distance[numeroSommetVoisin]){		//si la distance est plus petite on update et on push les valeurs 
							distance[numeroSommetVoisin] = distanceVoisin;
							predecesseur[numeroSommetVoisin] = sommetCourant;
							fileDePriorite.add(numeroSommetVoisin);
							troncon.put(numeroSommetVoisin,(ArrayList<Troncon>) troncon.get(sommetCourant).clone());
							troncon.get(numeroSommetVoisin).add(t);
							
						}
					}
					
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
		}
		//A la fin de la boucle, distance dispose des distances du sommet de d�part � tous les autres sommets, et predecesseur permet de suivre le chemin depus le noeud origine vers le noeud souhait�
		

		
		for(int i =0;i< noeudsArrivee.size();i++) {  //on parcourt les noeudsArrivee, on recupere le sommet correspondant pour recuperer la liste de troncon du sommet, puis on cr�e le trajet
			Noeud noeudCourant = noeudsArrivee.get(i);
			Long idCourant = noeudCourant.getId();
			Integer sommetCorrespondant = tableDeCorrespondanceIdSommet.get(idCourant);
			ArrayList<Troncon> listeTronconCourant = troncon.get(sommetCorrespondant);
			ensembleDijsktraaNoeuds.add(new Trajet(listeTronconCourant));
			
		}
		
		/*for (Map.Entry<Integer, ArrayList<Troncon>> entry : troncon.entrySet()) {
			
			System.out.println(tableDeCorrespondanceSommetId.get(entry.getKey()));
			
			for(Troncon t : entry.getValue()) {
				
				System.out.println(t.getNom());
				
			}
			
		}
		
		
		for(int i = 0;i<distance.length;i++) {
		
			System.out.println(predecesseur[i]);
			System.out.println(tableDeCorrespondanceSommetId.get(i));
			System.out.println(distance[i]);
		}*/
		
		return ensembleDijsktraaNoeuds;
	}
	
	public HashMap<Long,Noeud> getCollectionNoeuds() {
		return noeuds;
	}
	
	public void setCollectionNoeuds(HashMap<Long,Noeud>  noeuds) {
		this.noeuds = noeuds;
	}
	
	public ArrayList<Troncon> getCollectionTroncons() {
		return troncons;
	}
	
	public void setCollectionTroncons(ArrayList<Troncon> troncons) {
		this.troncons = troncons;
		setChanged();
		notifyObservers();
	}

	@Override
	public String toString() {
		return "Plan [noeuds=" + noeuds + ", troncons=" + troncons + "]";
	}	
	
	/*public static void main(String[] args) {
		
		
		Plan plan = new Plan();
		ArrayList<Noeud> listeNoeuds = new ArrayList<Noeud>();
		Noeud Paris = new Noeud();
		Noeud Marseille = new Noeud();
		Noeud Rennes = new Noeud();
		Noeud Avignon = new Noeud();
		Troncon ParisMarseille= new Troncon(1,"Troncon 12",Paris,Marseille);
		Troncon ParisRennes = new Troncon(20,"Troncon 13",Paris,Rennes);
		Troncon ParisAvignon = new Troncon(300,"Troncon 14",Paris,Avignon);
		Troncon MarseilleParis = new Troncon(4,"Troncon 21",Marseille,Paris);
		Troncon MarseilleRennes = new Troncon(50,"Troncon 23",Marseille,Rennes);
		Troncon MarseilleAvignon = new Troncon(600,"Troncon 24",Marseille,Avignon);
		Troncon RennesParis = new Troncon(7,"Troncon 31",Rennes,Paris);
		Troncon RennesMarseille = new Troncon(80,"Troncon 32",Rennes,Marseille);
		Troncon RennesAvignon = new Troncon(90,"Troncon 34",Rennes,Avignon);
		Troncon AvignonParis = new Troncon(10,"Troncon 41",Avignon,Paris);
		Troncon AvignonMarseille = new Troncon(110,"Troncon 42",Avignon,Marseille);
		Troncon AvignonRennes = new Troncon(1200,"Troncon 43",Avignon,Rennes);
		ArrayList<Troncon> tronconsParis = new ArrayList<>();
		ArrayList<Troncon> tronconsMarseille = new ArrayList<>();
		ArrayList<Troncon> tronconsRennes = new ArrayList<>();
		ArrayList<Troncon> tronconsAvignon = new ArrayList<>();
		tronconsParis.add(ParisMarseille);
		tronconsParis.add(ParisRennes);
		tronconsParis.add(ParisAvignon);
		tronconsMarseille.add(MarseilleParis);
		tronconsMarseille.add(MarseilleRennes);
		tronconsMarseille.add(MarseilleAvignon);
		tronconsRennes.add(RennesParis);
		tronconsRennes.add(RennesMarseille);
		tronconsRennes.add(RennesAvignon);
		tronconsAvignon.add(AvignonParis);
		tronconsAvignon.add(AvignonMarseille);
		tronconsAvignon.add(AvignonRennes);
		Paris.setTronconsPartants(tronconsParis);
		Marseille.setTronconsPartants(tronconsMarseille);
		Rennes.setTronconsPartants(tronconsRennes);
		Avignon.setTronconsPartants(tronconsAvignon);
		Paris.setId(new Long(1));
		Marseille.setId(new Long(2));
		Rennes.setId(new Long(3));
		Avignon.setId(new Long(4));
		HashMap<Long,Noeud>  HP = new HashMap<Long,Noeud>();
		HP.putIfAbsent(Paris.getId(),Paris);
		HP.putIfAbsent(Marseille.getId(),Marseille);
		HP.putIfAbsent(Rennes.getId(),Rennes);
		HP.putIfAbsent(Avignon.getId(),Avignon);
		listeNoeuds.add(Avignon);
		listeNoeuds.add(Marseille);
		listeNoeuds.add(Paris);
		listeNoeuds.add(Rennes);
		plan.setCollectionNoeuds(HP);
		
		ArrayList<Noeud> copieNoeuds =(ArrayList<Noeud>) listeNoeuds.clone();
		Collection<Trajet> recupTrajets = new ArrayList<Trajet>();
		for(Noeud n : listeNoeuds) {
			copieNoeuds.remove(n);
			recupTrajets = plan.calculerPlusCourtChemin(n,copieNoeuds);
			for(Trajet t : recupTrajets) {
				
				System.out.println(t.toString());
				
			}
			copieNoeuds.add(n);
			
		}
		
		
	}*/

	
}
