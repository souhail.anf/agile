package modele;
import java.util.ArrayList;

public class PointDeLivraison extends Noeud {
	
	private int duree;

	public PointDeLivraison(Long unId, double uneLatitude, double uneLongitude, ArrayList<Troncon> uneCollection, int uneDuree) {
		super(unId, uneLatitude, uneLongitude, uneCollection);
		this.setDuree(uneDuree);
	}
	
	public PointDeLivraison(Noeud noeud, int duree) {
		super(noeud.getId(),noeud.getLatitude(),noeud.getLongitude(),noeud.getTronconsPartants());
		this.duree = duree;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}
	
	public String toString()
	{
		String res = "PointDeLivraison = ";
		res += "Latitude = " + this.getLatitude();
		res += ", Longitude = " + this.getLongitude();
		res += ", Duree = "+ this.getDuree();
		
		return res;
	}
	
}
