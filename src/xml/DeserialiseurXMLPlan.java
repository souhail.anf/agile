package xml;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import modele.Noeud;
import modele.Plan;
import modele.Troncon;

public class DeserialiseurXMLPlan extends DeserialiseurXML{
	
	private static HashMap<Long,Noeud> collecNoeud = new HashMap<Long,Noeud>();
	private static ArrayList<Troncon> collecTroncon = new ArrayList<Troncon>();
	private static double minLat;
	private static double minLong;
	private static double maxLat;
	private static double maxLong;
	
	public static void chargerPlan(Plan plan) throws ExceptionXML {
		File xml = ouvrirFichier();
		try {
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
	        Document document = docBuilder.parse(xml);
	        Element racine = document.getDocumentElement();	        
	        if (racine.getNodeName().equals("reseau")) {
	        	initMinMax();
	            construireAPartirDeDOMXML(racine);
	            plan.setCollectionNoeuds(collecNoeud);
	            plan.setCollectionTroncons(collecTroncon);
	            plan.setLatmax(maxLat);
	            plan.setLatmin(minLat);
	            plan.setLongmax(maxLong);
	            plan.setLongmin(minLong);
	            /*System.out.println(maxLat);
	            System.out.println(minLat);
	            System.out.println(maxLong);
	            System.out.println(minLong);*/
	         }
	         else
	         	throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement du plan");
		}
 	}
	
	public static void chargerPlan(Plan plan, String filename) throws ExceptionXML {
		/*
		 * FOR TEST ONLY
		 * NE PAS UTILISER AILLEURS
		 */
		File xml = new File(filename);
		try {			
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
	        Document document = docBuilder.parse(xml);
	        Element racine = document.getDocumentElement();
	        if (racine.getNodeName().equals("reseau")) {
	        	initMinMax();
	            construireAPartirDeDOMXML(racine);
	            plan.setCollectionNoeuds(collecNoeud);
	            plan.setCollectionTroncons(collecTroncon);
	            plan.setLatmax(maxLat);
	            plan.setLatmin(minLat);
	            plan.setLongmax(maxLong);
	            plan.setLongmin(minLong);
	         }
	         else
	         	throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement du plan");
		}
 	}
	
	private static void construireAPartirDeDOMXML(Element noeudDOMRacine) throws Exception, NumberFormatException{
		
	   	NodeList listeNoeud = noeudDOMRacine.getElementsByTagName("noeud");
	   	for (int i = 0; i < listeNoeud.getLength(); i++) {
	   		Element el = (Element) (listeNoeud.item(i));
	   		Noeud noeudTempo = creerNoeud(el);
	   		collecNoeud.put(noeudTempo.getId(), noeudTempo);
	   	}
	   	
	   	NodeList listeNoeudTroncon = noeudDOMRacine.getElementsByTagName("troncon");
	   	for (int i = 0; i < listeNoeudTroncon.getLength(); i++) {
	   		Element el = (Element) (listeNoeudTroncon.item(i));
	   		Troncon tronconTempo = creerTroncon(el);
	   		collecTroncon.add(tronconTempo);
	   	}
	}
	
	private static Noeud creerNoeud(Element elt) throws Exception {
		Noeud noeudTempo = null;
		
		double longitude = Double.parseDouble(elt.getAttribute("longitude"));
		if(longitude<=0) 
			throw new Exception("Longitude n�gative");
		
   		double latitude = Double.parseDouble(elt.getAttribute("latitude"));
   		if(latitude<=0) 
   			throw new Exception("Latitude n�gative");
   		
   		long id = Long.parseLong(elt.getAttribute("id"));
   		if(id==0) 
   			throw new Exception("Id incorrect");
   		
   		noeudTempo = new Noeud(id,latitude,longitude,null);
   		
   		if(latitude > maxLat)
   			maxLat = latitude;   		
   		if(latitude < minLat)
   			minLat = latitude;
   		if(longitude > maxLong)
   			maxLong = longitude;   		
   		if(longitude < minLong)   			
   			minLong = longitude;
   		
   			
   			
   		return noeudTempo;
	}
	
	private static Troncon creerTroncon(Element elt) throws Exception {
		Troncon tronconTempo = null;
		
		double longueur = Double.parseDouble(elt.getAttribute("longueur"));
		if (longueur<0)
			throw new Exception("Longueur n�gative");
   		String nomRue = elt.getAttribute("nomRue");
   		long idNoeudOrigine = Long.parseLong(elt.getAttribute("origine"));
   		if(idNoeudOrigine==0) 
   			throw new Exception("idNoeudOrigine incorrect");
   		
   		long idNoeudDestination = Long.parseLong(elt.getAttribute("destination"));
   		if(idNoeudDestination==0) 
   			throw new Exception("idNoeudDestination incorrect");
   		

   		Noeud NoeudOrigine = getNoeudDansCollectionByID(idNoeudOrigine);
   		Noeud NoeudDestination = getNoeudDansCollectionByID(idNoeudDestination);
   		
   		tronconTempo = new Troncon(longueur,nomRue,NoeudOrigine,NoeudDestination);
   		
   		NoeudOrigine.addTronconPartant(tronconTempo);
   		
   		return tronconTempo;
	}
	
	private static Noeud getNoeudDansCollectionByID(Long myId)
	{
		Noeud noeudResultat = collecNoeud.get(myId);
		return noeudResultat;
	}
	
	private static void initMinMax()
	{
		
		maxLong = 0.0;
		minLong = 9999999.0;
		maxLat = 0.0;
		minLat = 99999999.0;
	}
}
