package xml;

@SuppressWarnings("serial")
public class ExceptionXML extends Exception {

	public ExceptionXML(String message) {
		super(message);
	}
}
