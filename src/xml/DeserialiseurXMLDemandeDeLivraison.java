package xml;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import modele.DemandeDeLivraison;
import modele.Entrepot;
import modele.Noeud;
import modele.Plan;
import modele.PointDeLivraison;

public class DeserialiseurXMLDemandeDeLivraison extends DeserialiseurXML {

	private static ArrayList<PointDeLivraison> collecPointDeLivraison = new ArrayList<PointDeLivraison>();
	private static Entrepot entrepot = null;
	private static SimpleDateFormat monFormat = new SimpleDateFormat("hh:mm:ss");

	public static void chargerDemandeDeLivraison(DemandeDeLivraison dedel, Plan plan) throws Exception {
		try {
			File xml = ouvrirFichier();
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
			Document document = docBuilder.parse(xml);
			Element racine = document.getDocumentElement();
			if (racine.getNodeName().equals("demandeDeLivraisons")) {
				construireAPartirDeDOMXML(racine, plan);
				//dedel = new DemandeDeLivraison(entrepot,collecPointDeLivraison);			
				dedel.setEntrepot(entrepot);
				dedel.setPointsDeLivraison(collecPointDeLivraison);
			}
			else
				throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement du plan");
		}
	}
	
	public static void chargerDemandeDeLivraison(DemandeDeLivraison dedel, Plan plan, String filename) throws Exception {
		/*
		 * FOR TEST ONLY
		 * NE PAS UTILISER AILLEURS
		 */
		try {
			File xml = new File(filename);
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();	
			Document document = docBuilder.parse(xml);
			Element racine = document.getDocumentElement();
			if (racine.getNodeName().equals("demandeDeLivraisons")) {
				construireAPartirDeDOMXML(racine, plan);
				//dedel = new DemandeDeLivraison(entrepot,collecPointDeLivraison);			
				dedel.setEntrepot(entrepot);
				dedel.setPointsDeLivraison(collecPointDeLivraison);
			}
			else
				throw new Exception("Document non conforme");
		} catch (Exception e) {
			throw new ExceptionXML("Erreur de chargement du plan");
		}
	}

	private static void construireAPartirDeDOMXML(Element noeudDOMRacine, Plan plan) throws Exception, NumberFormatException{

		NodeList listeNoeud = noeudDOMRacine.getElementsByTagName("entrepot");
		for (int i = 0; i < listeNoeud.getLength(); i++) {
			Element el = (Element) (listeNoeud.item(i));
			Entrepot entrepotTempo = creerEntrepot(el, plan);
			entrepot = entrepotTempo;
		}

		NodeList listeNoeudTroncon = noeudDOMRacine.getElementsByTagName("livraison");
		for (int i = 0; i < listeNoeudTroncon.getLength(); i++) {
			Element el = (Element) (listeNoeudTroncon.item(i));
			PointDeLivraison pointDeLivraisonTempo = creerPointDeLivraison(el, plan);
			collecPointDeLivraison.add(pointDeLivraisonTempo);
		}
	}

	private static Entrepot creerEntrepot(Element elt, Plan plan) throws Exception {
		Entrepot EntrepotTempo = null;
		String heureDepart = elt.getAttribute("heureDepart");
		Date date = monFormat.parse(heureDepart);
		long IdNoeud = Long.parseLong(elt.getAttribute("adresse"));
		Noeud noeud = plan.rechercheNoeudParId(IdNoeud);
		EntrepotTempo = new Entrepot(noeud,date);

		return EntrepotTempo;

	}

	private static PointDeLivraison creerPointDeLivraison(Element elt, Plan plan) throws Exception {
		PointDeLivraison pointDeLivraisonTempo = null;
		long IdNoeud = Long.parseLong(elt.getAttribute("adresse"));
		int duree = Integer.parseInt(elt.getAttribute("duree"));
		Noeud noeud = plan.rechercheNoeudParId(IdNoeud);
		pointDeLivraisonTempo = new PointDeLivraison(noeud,duree);
		
		return pointDeLivraisonTempo;
	}

}
