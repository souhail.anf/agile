package xml;
import java.io.File;

public abstract class DeserialiseurXML {
	
	protected static File ouvrirFichier() throws ExceptionXML {
		File xml = OuvreurDeFichierXML.getInstance().ouvre(true);
		return xml;
		
	}
	
}
