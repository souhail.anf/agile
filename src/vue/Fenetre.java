package vue;

import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Plan;
import controleur.Controleur;
import vue.VueGraphique;

public class Fenetre extends JFrame {
	
	// Intitules des boutons de la fenetre
	protected final static String CHARGER_PLAN = "Charger un plan";
	protected static final String CHARGER_DEMANDE_DE_LIVRAISON = "Charger une demande de livraison";
	protected final static String RENSEIGNER_NB_LIVREURS = "Renseigner le nombre de livreurs";
	protected final static String CALCULER_TOURNEES = "Calculer les tournees";
	private ArrayList<JButton> boutons;
	private JLabel cadreMessages;
	private JLabel cadreLivreur;
	/*private JTextArea inputNbLivreur;*/
	private VueGraphique vueGraphique;
	private VueTextuelle vueTextuelle;
	private JScrollPane scrollTexte;
	
	private EcouteurDeBoutons ecouteurDeBoutons;
	private EcouteurDeSouris ecouteurDeSouris;
	
	private final String[] intitulesBoutons = new String[]{CHARGER_PLAN, CHARGER_DEMANDE_DE_LIVRAISON, 
			RENSEIGNER_NB_LIVREURS,CALCULER_TOURNEES};
	private final int hauteurBouton = 40;
	private final int largeurBouton = 230;
	private final int hauteurCadreMessages = 80;
	private final int hauteurCadreLivreurs = 50;
	private final int largeurVueTextuelle = 400;
	
	private int valueLivreur = 0;

	
	/**
	 * Cree une fenetre avec des boutons, une zone graphique pour dessiner le plan plan avec la demande de livraison demandeDeLivraison, 
	 * un cadre pour afficher des messages, une zone textuelle decrivant les formes de p,
	 * et des ecouteurs de boutons, de clavier et de souris qui envoient des messages au controleur c
	 * @param plan le plan
	 * @param demandeDeLivraison la demande de livraison
	 * @param controleur le controleur
	 */
	public Fenetre(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees edt, Controleur controleur){
		setLayout(null);
		creeBoutons(controleur);
		
		cadreMessages = new JLabel();
		cadreMessages.setBorder(BorderFactory.createTitledBorder("Messages..."));
		
		cadreLivreur = new JLabel();
		cadreLivreur.setBorder(BorderFactory.createTitledBorder("Nb Livreurs :"));	
		cadreLivreur.setText(Integer.toString(valueLivreur));
		//inputNbLivreur = new JTextArea();
		
		
		getContentPane().add(cadreMessages);
		getContentPane().add(cadreLivreur);
		//getContentPane().add(inputNbLivreur);
		
		vueGraphique = new VueGraphique(plan, demandeDeLivraison, edt, this);		
		vueTextuelle = new VueTextuelle(plan, demandeDeLivraison, edt, this);
		
		ecouteurDeSouris = new EcouteurDeSouris(controleur,vueGraphique,this);
		addMouseListener(ecouteurDeSouris);
		addMouseMotionListener(ecouteurDeSouris);
		
		scrollTexte = new JScrollPane(vueTextuelle,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scrollTexte);
		setTailleFenetre();
		setVisible(true);
	}

	/**
	 * Cree les boutons correspondant aux intitules contenus dans intitulesBoutons
	 * cree un ecouteur de boutons qui ecoute ces boutons
	 * @param controleur
	 */
	private void creeBoutons(Controleur controleur){
		ecouteurDeBoutons = new EcouteurDeBoutons(controleur, this);
		boutons = new ArrayList<JButton>();
		for (String intituleBouton : intitulesBoutons){
			JButton bouton = new JButton(intituleBouton);
			boutons.add(bouton);
			bouton.setSize(largeurBouton,hauteurBouton);
			bouton.setLocation(0,(boutons.size()-1)*hauteurBouton);
			bouton.setFocusable(false);
			bouton.setFocusPainted(false);
			bouton.addActionListener(ecouteurDeBoutons);
			getContentPane().add(bouton);	
		}
	}
	
	/**
	 * Definit la taille du cadre et de ses composants en fonction de la taille de la vue
	 * @param largeurVue
	 * @param hauteurVue
	 */
	private void setTailleFenetre() {
		int hauteurBoutons = hauteurBouton*intitulesBoutons.length;
		int hauteurFenetre = Math.max(vueGraphique.getHauteur(),hauteurBoutons)+hauteurCadreMessages;
		int largeurFenetre = vueGraphique.getLargeur()+largeurBouton+largeurVueTextuelle+10;
		setSize(largeurFenetre, hauteurFenetre);
		
		cadreMessages.setSize(largeurFenetre,60);
		cadreMessages.setLocation(0,hauteurFenetre-hauteurCadreMessages);
		
		cadreLivreur.setSize(largeurBouton,hauteurCadreLivreurs);
		cadreLivreur.setLocation(0,hauteurFenetre-hauteurCadreMessages-hauteurCadreLivreurs);
		/*inputNbLivreur.setSize(largeurBouton-35,hauteurCadreLivreurs/2 -10);
		inputNbLivreur.setLocation(30,hauteurFenetre-hauteurCadreMessages-(hauteurCadreLivreurs/2)-2);*/
		
		vueGraphique.setLocation(largeurBouton, 0);
		vueTextuelle.setSize(largeurVueTextuelle,hauteurFenetre-hauteurCadreMessages);
		vueTextuelle.setLocation(10+vueGraphique.getLargeur()+largeurBouton,0);
		scrollTexte.setSize(largeurVueTextuelle,hauteurFenetre-hauteurCadreMessages);
		scrollTexte.setLocation(10+vueGraphique.getLargeur()+largeurBouton,0);
	}

	/**
	 * Affiche message dans la fenetre de dialogue avec l'utilisateur
	 * @param message
	 */
	public void afficheMessage(String message) {
		cadreMessages.setText(message);
	}	
	
	/**
	 * Active les boutons si b = true, les desactive sinon
	 * @param b
	 */
	public void autoriseBoutons(Boolean b) {
		for (JButton bouton : boutons)
			bouton.setEnabled(b);
	}
	
	public int getEchelle(){
		return vueGraphique.getEchelle();
	}
	
	public void setEchelle(int echelle){
		vueGraphique.setEchelle(echelle);
		setTailleFenetre();
	}
	
	public void lancerInterfaceConfigNbLivreurs(){
		Object[] possibilities  = {"1", "2", "3", "4","5","6","7","8","9","10"};
		try{
			int nbPointDeLivraison = vueGraphique.getNbPointsDeLivraison();
			possibilities = new Object[nbPointDeLivraison];		
			for(int i=0; i<nbPointDeLivraison; i++)
			{
				possibilities[i] = Integer.toString(i+1);
			}
		}catch(Exception e){
			
		}		
		
		String s = (String)JOptionPane.showInputDialog(
		                    this,
		                    "Veuillez renseigner le nombre de livreurs:\n"
		                    ,
		                    "Configuration du nombre de livreurs",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    possibilities,
		                    "1");
		try{
			valueLivreur = Integer.parseInt(s);
		}
		catch(Exception e)
		{
			
		}	
		
	}
	
	public void setNbLivreursDsFenetre(int nbLivreur){
		valueLivreur = nbLivreur;
		cadreLivreur.setText(Integer.toString(valueLivreur));
	}
	
	/*public int getInputNbLivreur(){	
		String text = inputNbLivreur.getText();
		int res;
		try{
			 res = Integer.parseInt(text);
			 
		}catch(Exception e){
			res = 0 ;
		}
		
		return res;
	}*/
	
	public int getValueNbLivreur(){
		return valueLivreur;
	}
	
}
