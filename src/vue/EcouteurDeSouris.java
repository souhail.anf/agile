package vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import controleur.Controleur;

public class EcouteurDeSouris extends MouseAdapter {

	private Controleur controleur;
	private VueGraphique vueGraphique;
	private Fenetre fenetre;

	public EcouteurDeSouris(Controleur controleur, VueGraphique vueGraphique, Fenetre fenetre){
		this.controleur = controleur;
		this.vueGraphique = vueGraphique;
		this.fenetre = fenetre;
	}

	@Override
	public void mouseClicked(MouseEvent evt) {		
		switch (evt.getButton()){
			//bouton gauche de la souris
			case MouseEvent.BUTTON1:
				int x = coordonneesX(evt);
				int y = coordonneesY(evt);
				System.out.println("Clic Gauche"+"x= "+x+"y= "+y);
				break;
				
			//bouton droit de la souris	
			case MouseEvent.BUTTON3: 
				
				break;
		
		default:
		}
	}

	public void mouseMoved(MouseEvent evt) {
		// Methode appelee a chaque fois que la souris est bougee
		// Envoie au controleur les coordonnees de la souris.
		int x = coordonneesX(evt);
		int y = coordonneesY(evt);
		if (x != 0 && y != 0)
		{
			//controleur.sourisBougee(p); 
			//System.out.println("x= "+x+"y= "+y);
		}
	}
	
	private int coordonneesX(MouseEvent evt){
		MouseEvent e = SwingUtilities.convertMouseEvent(fenetre, evt, vueGraphique);
		int x = Math.round((float)e.getX()/*/(float)vueGraphique.getEchelle()*/);
		
		return x;
	}
	
	private int coordonneesY(MouseEvent evt){
		MouseEvent e = SwingUtilities.convertMouseEvent(fenetre, evt, vueGraphique);
		int y = Math.round((float)e.getY()/*/(float)vueGraphique.getEchelle()*/);
		
		return y;
	}	
	
}