package vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import controleur.Controleur;

public class EcouteurDeBoutons implements ActionListener {
	
	private Controleur controleur;
	private Fenetre fenetre;

	public EcouteurDeBoutons(Controleur controleur,Fenetre fen){
		this.controleur = controleur;
		this.fenetre = fen;
	}

	@Override
	public void actionPerformed(ActionEvent e) { 
		// Methode appelee par l'ecouteur de boutons a chaque fois qu'un bouton est clique
		// Envoi au controleur du message correspondant au bouton clique
		switch (e.getActionCommand()){
			case Fenetre.CHARGER_PLAN: controleur.chargerPlan(); break;
			case Fenetre.CHARGER_DEMANDE_DE_LIVRAISON: controleur.chargerLivraisons(); break;
			case Fenetre.RENSEIGNER_NB_LIVREURS: 
				fenetre.lancerInterfaceConfigNbLivreurs();
				controleur.renseignerNbLivreurs(); 				
				break;
		}
	}
}
