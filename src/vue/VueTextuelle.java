package vue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Entrepot;
import modele.Plan;
import modele.PointDeLivraison;
import modele.Troncon;

public class VueTextuelle extends JLabel implements Observer{

	private String texte;
	private Plan plan;
	private DemandeDeLivraison demandeDeLivraison;
	private EnsembleDeTournees ensembleDeTournees;
	
	/**
	 * Cree une vue textuelle de plan dans fenetre
	 * @param plan
	 * @param fenetre
	 */
	public VueTextuelle(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees ensembleDeTournees, Fenetre fenetre){
		super();
		setBorder(BorderFactory.createTitledBorder("Carte textuelle : "));		
		this.setVerticalTextPosition(TOP);
		this.setVerticalAlignment(TOP);
		fenetre.getContentPane().add(this);		
		plan.addObserver(this); // this observe plan
		demandeDeLivraison.addObserver(this); //this observes dedel		
		ensembleDeTournees.addObserver(this);
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		this.ensembleDeTournees = ensembleDeTournees;
	}
	
	/**
	 * Methode appelee par les objets observes par this a chaque fois qu'ils ont ete modifies
	 */
	@Override
	public void update(Observable o, Object arg) {
		//TO DO AS
		/*if (arg != null){ // arg est une forme qui vient d'etre ajoutee a plan
			Forme f = (Forme)arg;
			f.addObserver(this);
		}
		Iterator<Forme> it = plan.getIterateurFormes();*/
		texte = "<html>";	
		
		Entrepot entrepot = demandeDeLivraison.getEntrepot();
		if(entrepot != null)
		{
			texte += "<h2> Entrepot </h2>";
			texte += entrepot.toString();
		}
		
		ArrayList<PointDeLivraison> pointsDeLivraison = demandeDeLivraison.getPointsDeLivraison();
		if(pointsDeLivraison != null && pointsDeLivraison.size()>0)
		{			
			texte += "<div> <h2> Points De Livraisons </h2> <ul>";
			for(int i=0; i<pointsDeLivraison.size(); i++)
			{
				texte += "<li>" + pointsDeLivraison.get(i).toString() + "</li>";				
			}
			texte += "</ul> </div>";
		}		
		
		texte = texte+"</html>";
		setText(texte);		
	}
	
	public void affiche(Troncon  t) {
		//TO DO AS
		/*
		texte = texte+"<li>";
		if (c.getEstSelectionne())
			texte = texte+"<b><i>";
		texte = texte+"Cercle : centre=(" + c.getCentre().getX() 
				+ "," + c.getCentre().getY()
				+ ") rayon=" + c.getRayon();
		if (c.getEstSelectionne())
				texte = texte+"</i></b>";
		texte = texte+"</li>";*/
	}
	
}
