package vue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import modele.*;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class VueGraphique extends JPanel implements Observer{

	private int echelle;
	private int hauteurVue;
	private int largeurVue;
	private Plan plan;
	private DemandeDeLivraison demandeDeLivraison;
	private EnsembleDeTournees ensembleDeTournees;
	private Graphics g;

	/**
	 * Cree la vue graphique permettant de dessiner plan avec l'echelle e dans la fenetre f
	 * @param plan
	 * @param e l'echelle
	 * @param f la fenetre
	 */
	public VueGraphique(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees ensembleDeTournees, Fenetre f) {
		super();
		plan.addObserver(this); // this observe plan
		demandeDeLivraison.addObserver(this); //this observe demandeDeLivraison
		ensembleDeTournees.addObserver(this);
		this.echelle = 10;
		hauteurVue = plan.getHauteurplan()*echelle;
		largeurVue = plan.getLargeurplan()*echelle;
		setLayout(null);
		setBackground(Color.white);
		setSize(largeurVue, hauteurVue);
		f.getContentPane().add(this);
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		this.ensembleDeTournees = ensembleDeTournees;
	}
	
	/**
	 * Methode appelee a chaque fois que VueGraphique doit etre redessinee
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.g = g;		
		
		ArrayList<Troncon> troncons = plan.getCollectionTroncons();
		if(troncons != null)
		{			
			for(int i=0; i<troncons.size(); i++)
			{
				affiche(troncons.get(i));				
			}			
		}
		
		ArrayList<PointDeLivraison> pointsDeLivraison = demandeDeLivraison.getPointsDeLivraison();
		if(pointsDeLivraison != null)
		{			
			for(int i=0; i<pointsDeLivraison.size(); i++)
			{
				affiche(pointsDeLivraison.get(i));				
			}			
		}
		
		Entrepot entrepot = demandeDeLivraison.getEntrepot();
		if(entrepot != null)
		{
			affiche(entrepot);
		}
		
		ArrayList<Tournee> tournees = ensembleDeTournees.getListeTournee();
		if(tournees != null)
		{
			for(int i=0; i<tournees.size(); i++)
			{
				affiche(tournees.get(i));
			}
		}
	}

	public void setEchelle(int e) {
		largeurVue = (largeurVue/echelle)*e;
		hauteurVue = (hauteurVue/echelle)*e;
		setSize(largeurVue, hauteurVue);
		echelle = e;
	}

	public int getEchelle() {
		return echelle;
	}

	public int getHauteur() {
		return hauteurVue;
	}

	public int getLargeur() {
		return largeurVue;
	}
	
	public int getNbPointsDeLivraison() {
		return demandeDeLivraison.getPointsDeLivraison().size();
	}

	/**
	 * Methode appelee par les objets observes par this a chaque fois qu'ils ont ete modifies
	 */
	@Override
	public void update(Observable o, Object arg) {
		/*if (arg != null){ // arg est une forme qui vient d'etre ajoutee a plan
			Forme f = (Forme)arg;
			f.addObserver(this);  // this observe la forme f
		}*/		
		repaint();
	}
	
	public void affiche(Troncon t) {
		//changer le design si il est selectionne
		int xori = convertirLatitude(t.getNoeudOrigine().getLatitude());
		int yori = convertirLongitude(t.getNoeudOrigine().getLongitude());
		int xdest = convertirLatitude(t.getNoeudDestination().getLatitude());
		int ydest = convertirLongitude(t.getNoeudDestination().getLongitude());		
		g.drawLine(yori, xori,ydest , xdest);
		//System.out.println(xori);
	}
	
	public void affiche(Noeud n) {
		//changer le design si il est selectionne
	}
	
	public void affiche(Tournee t) {
		//changer le design si il est selectionne
		
	}
	public void affiche(PointDeLivraison l) {
		
		int ynoeud = convertirLatitude(l.getLatitude());
		int xnoeud = convertirLongitude(l.getLongitude());	
		g.setColor(Color.red);
		g.fillOval(xnoeud-5,ynoeud-5,10,10);
	}
	public void affiche(Entrepot p){
		//changer le design si il est selectionne
		int ynoeud = convertirLatitude(p.getLatitude());
		int xnoeud = convertirLongitude(p.getLongitude());	
		g.setColor(Color.blue);
		g.fillOval(xnoeud-5,ynoeud-5,10,10);
		System.out.println(xnoeud);
	}
	public void affiche(Trajet t){
		//changer le design si il est selectionne
	}
	
	public int convertirLatitude(double lat)
	{
		double rapportLatEchelle = plan.getLatmax()-plan.getLatmin();
		double rapportAuMinLat = lat-plan.getLatmin();
		int resultat = (int)((rapportAuMinLat/rapportLatEchelle)* plan.getLargeurplan()*echelle);		
		return (plan.getLargeurplan()*echelle-resultat);
	}
	
	public int convertirLongitude(double longi)
	{
		double rapportLongEchelle = plan.getLongmax()-plan.getLongmin();
		double rapportAuMinLong = longi - plan.getLongmin();
		int resultat = (int)((rapportAuMinLong/rapportLongEchelle)*plan.getHauteurplan()*echelle);		
		//return (plan.getHauteurplan()*echelle-resultat);
		return resultat;
	}
	
}
