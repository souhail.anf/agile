package tsp;

import java.util.ArrayList;
import java.util.Iterator;

public class TSP1 extends TemplateTSP {

	@Override
	protected Iterator<Integer> iterator(Integer sommetCrt, ArrayList<Integer> nonVus, double[][] cout, int[] duree) {
		return new IteratorSeq(nonVus, sommetCrt);
	}

	@Override
	protected int bound(Integer sommetCourant, ArrayList<Integer> nonVus, double[][] cout, int[] duree) {
		return 0;
	}
	
	public static void main(String[] args) {
		
		/*
		 * Exemple fait avec les villes suivante
		 * 
		 * 0 Lille
		 * 1 Paris
		 * 2 Lyon
		 * 3 Marseille
		 */
		
		int tpsLimite = 1000;
		
		int nbSommets = 4;
		
		double[][] cout = {
				{ 0, 10, 30, 50},
				{10,  0, 20, 40},
				{30, 20,  0, 20},
				{50, 40, 20,  0}
		};
		
		int[] duree = {0, 0, 0, 0};
		
		TSP1 tsp = new TSP1();
		tsp.chercheSolution(tpsLimite, nbSommets, cout, duree);
		
		while(tsp.getTempsLimiteAtteint()) {
			System.out.println("Calcul en court");
		}

		double coutMeilleureSolution = tsp.getCoutMeilleureSolution();
		System.out.println(coutMeilleureSolution);
		
		for(int i=0 ; i<nbSommets ; i++) {
				int meilleurSolution = tsp.getMeilleureSolution(i);
				System.out.println(meilleurSolution);
		}
	}
}
