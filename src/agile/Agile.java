package agile;

import java.util.ArrayList;

import controleur.Controleur;
import modele.*;

public class Agile {
	
	private static final int largeurPlan = 50;
	private static final int hauteurPlan = 50;
	
	public static void main(String[] args) {
		Plan plan = new Plan(largeurPlan, hauteurPlan);		
		DemandeDeLivraison demandeDeLivraison = new DemandeDeLivraison();	
		EnsembleDeTournees esd = new EnsembleDeTournees();
		new Controleur(plan, demandeDeLivraison, esd);
	}
	
	//Methode pour tester la vue
	public static void simulTestPlan(Plan p, DemandeDeLivraison dedel)
	{
		ArrayList<Troncon> testTroncons = new ArrayList<Troncon>();
		Noeud n1 = new Noeud(new Long(25175791),(double)45.75406 , (double)4.857418,null);
		Noeud n2 = new Noeud(new Long(25175792), (double)45.750404,(double)4.8744674,null);
		Noeud n3 = new Noeud(new Long(25175793),(double)45.75871 ,(double)4.8704023  ,null);
		Troncon t1 = new Troncon((double)10, "Rue 1", n1,n2);
		Troncon t2 = new Troncon((double)10, "Rue 2", n2,n3);
		Troncon t3 = new Troncon((double)10, "Rue 3", n3,n1);
		testTroncons.add(t1);
		testTroncons.add(t2);
		testTroncons.add(t3);
		p.setCollectionTroncons(testTroncons);
		
		ArrayList<PointDeLivraison> pointsDeLivraison = new ArrayList<PointDeLivraison>();
		PointDeLivraison p1 = new PointDeLivraison(new Long(0),45.75406  ,4.857418,null, 10);
		PointDeLivraison p2 = new PointDeLivraison(new Long(0),45.750404  ,4.8744674,null, 10);		
		pointsDeLivraison.add(p2);
		pointsDeLivraison.add(p1);
		dedel.setPointsDeLivraison(pointsDeLivraison);	
		
	}

}
