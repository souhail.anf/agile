package controleur;

import modele.Plan;
import vue.Fenetre;
import xml.DeserialiseurXMLPlan;

public class EtatInit extends EtatDefaut {

	@Override
	public void chargerPlan(Plan plan, Fenetre fenetre, Controleur c) {
		try {
			DeserialiseurXMLPlan.chargerPlan(plan);
			c.setEtatCourant(c.etatPlanCharge);
			fenetre.afficheMessage("Plan charg�");
		} catch (Exception e) {
			fenetre.afficheMessage("Erreur lors de l'ouverture du plan");
			e.printStackTrace();
		}
	}
}