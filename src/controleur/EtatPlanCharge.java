package controleur;

import modele.DemandeDeLivraison;
import modele.Plan;
import vue.Fenetre;
import xml.DeserialiseurXMLDemandeDeLivraison;

public class EtatPlanCharge extends EtatDefaut {

	@Override
	public void chargerLivraisons(DemandeDeLivraison demandeDeLivraison, Plan plan, Fenetre fenetre, Controleur c) {
		try {
			DeserialiseurXMLDemandeDeLivraison.chargerDemandeDeLivraison(demandeDeLivraison, plan);
			c.setEtatCourant(c.etatLivraisonsChargees);
			fenetre.afficheMessage("Livraisons charg�es");
		} catch (Exception e) {
			e.printStackTrace();
			fenetre.afficheMessage("Erreur lors du chargement des livraisons");
		}
	}
}
