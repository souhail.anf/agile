package controleur;

import modele.DemandeDeLivraison;
import modele.Plan;
import vue.Fenetre;

public class EtatDefaut implements Etat {

	@Override
	public void chargerPlan(Plan plan, Fenetre fenetre, Controleur c) {		
	}

	@Override
	public void chargerLivraisons(DemandeDeLivraison demandeDeLivraison, Plan plan, Fenetre fenetre, Controleur c) {		
	}

	@Override
	public void renseignerNbLivreurs(DemandeDeLivraison d,/* int nb,*/ Fenetre fenetre) {
		d.setNombreLivreurs(fenetre.getValueNbLivreur());
		fenetre.setNbLivreursDsFenetre(fenetre.getValueNbLivreur());
	}
	@Override
	public void calculerTournees(DemandeDeLivraison dedel) {};

}
