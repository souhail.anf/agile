package controleur;

import modele.DemandeDeLivraison;
import modele.EnsembleDeTournees;
import modele.Plan;
import vue.Fenetre;

public class Controleur {
	private Etat etatCourant;

	// Objets du modèle
	private Plan plan;
	private DemandeDeLivraison demandeDeLivraison;
	private Fenetre fenetre;

	protected final EtatInit etatInit = new EtatInit();
	protected final EtatLivraisonsChargees etatLivraisonsChargees = new EtatLivraisonsChargees();
	protected final EtatPlanCharge etatPlanCharge = new EtatPlanCharge();


	public Controleur(Plan plan, DemandeDeLivraison demandeDeLivraison, EnsembleDeTournees esd) {
		etatCourant = etatInit;
		this.plan = plan;
		this.demandeDeLivraison = demandeDeLivraison;
		fenetre = new Fenetre(plan,demandeDeLivraison, esd, this);
	}

	public void chargerPlan(){
		etatCourant.chargerPlan(plan, fenetre, this);
	}
	public void chargerLivraisons(){
		etatCourant.chargerLivraisons(demandeDeLivraison, plan, fenetre, this);
	}
	
	public void renseignerNbLivreurs(){
		etatCourant.renseignerNbLivreurs(demandeDeLivraison, fenetre);
	}
	
	public void setEtatCourant(Etat etat) {
		etatCourant = etat;
	}
	
	public void chargerTournees() {
		etatCourant.calculerTournees(demandeDeLivraison);
	}
}
