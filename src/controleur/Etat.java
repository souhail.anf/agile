package controleur;

import modele.DemandeDeLivraison;
import modele.Plan;
import vue.Fenetre;

public interface Etat {

	
	public void chargerPlan(Plan plan, Fenetre fenetre, Controleur c);
	public void chargerLivraisons(DemandeDeLivraison demandeDeLivraison, Plan plan, Fenetre fenetre, Controleur c);
	public void renseignerNbLivreurs(DemandeDeLivraison d,/* int nb,*/ Fenetre fenetre);
	public void calculerTournees(DemandeDeLivraison dedel);
}
