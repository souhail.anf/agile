package controleur;

import modele.DemandeDeLivraison;

public class EtatLivraisonsChargees extends EtatDefaut {
	@Override
	public void calculerTournees(DemandeDeLivraison dedel) {
		dedel.calculerEnsembleLivraisons();
	}
}
