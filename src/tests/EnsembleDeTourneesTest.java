package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.EnsembleDeTournees;
import modele.Entrepot;
import modele.Noeud;
import modele.Tournee;

class EnsembleDeTourneesTest {

	@Test
	void test() {
		//Tests sur constructeur de EnsembleDeTournees
		ArrayList<Tournee> listeTournee = new ArrayList<Tournee>();
		Noeud noeud = new Noeud();
		Entrepot unEntrepot = new Entrepot(noeud, null);
		EnsembleDeTournees edt = new EnsembleDeTournees(listeTournee,unEntrepot);
		assertNotNull(edt.getEntrepot());
		assertNotNull(edt.getListeTournee());
	}

}
