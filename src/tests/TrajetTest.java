package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Trajet;
import modele.Troncon;

class TrajetTest {

	@Test
	void test() {
		//Tests sur constructeur de Trajet
		ArrayList<Troncon> parcoursTroncons = new ArrayList<Troncon>();
		double uneLongueur = 14.3;
		String unNom = "nom";
		Noeud uneOrigine  = new Noeud();
		Noeud uneDestination = new Noeud();
		Troncon t1 = new Troncon(uneLongueur, unNom, uneOrigine, uneDestination);
		Troncon t2 = new Troncon(uneLongueur, unNom, uneOrigine, uneDestination);
		parcoursTroncons.add(t1);
		parcoursTroncons.add(t2);
		Trajet t = new Trajet(parcoursTroncons);
		assertNotNull(t.getNoeudArrivee());
		assertNotNull(t.getNoeudDepart());
		assertNotNull(t.getParcoursTroncons());
		assertEquals(t.getLongueur(), 28.6);
	}

}
