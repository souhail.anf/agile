package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.Test;

import modele.Entrepot;
import modele.Noeud;
import modele.Troncon;

class EntrepotTest {

	@Test
	void test() {
		//Tests sur constructeur complet de Entrepot 
		Long id = new Long(12);
		double latitude = 12.0;
		double longitude = 14.2;
		Date heureDepart = new Date();
		ArrayList<Troncon> tronconsPartants = new ArrayList<Troncon>();
		Entrepot entrepot = new Entrepot(id, latitude, longitude, tronconsPartants, heureDepart);
		assertNotNull(entrepot.getTronconsPartants());
		assertNotNull(entrepot.getHeureDepart());
		assertEquals(entrepot.getLatitude(), 12.0);
		assertEquals(entrepot.getLongitude(), 14.2);
				
		//Tests sur constructeur avec Noeud de Entrepot 
		Noeud noeud = new Noeud(id,latitude,longitude,tronconsPartants);
		Entrepot entrepot2 = new Entrepot(noeud, heureDepart);
		assertNotNull(entrepot2.getTronconsPartants());
		assertNotNull(entrepot2.getHeureDepart());
		assertEquals(entrepot2.getLatitude(), 12.0);
		assertEquals(entrepot2.getLongitude(), 14.2);
		
	
	}

}
