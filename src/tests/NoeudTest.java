package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Troncon;

class NoeudTest {

	@Test
	void test() {
		//Tests sur constructeur de Noeud
		Long id = new Long(12);
		double latitude = 12.0;
		double longitude = 14.2;
		Noeud noeud = new Noeud(id,latitude,longitude,null);
		assertNotNull(noeud.getTronconsPartants());
		assertEquals(noeud.getLatitude(), 12.0);
		assertEquals(noeud.getLatitude(), 12.0);
		assertEquals(noeud.getLongitude(), 14.2);
		
		//Tests sur calculDistanceEuclidienne
		//TO DO
		
		//Tests sur addTroncons
		Troncon t = new Troncon();
		noeud.addTronconPartant(t);
		assertEquals(noeud.getTronconsPartants().size(), 1);
	}

}
