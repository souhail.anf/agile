package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import modele.Pair;
import modele.PointDeLivraison;
import modele.Tournee;
import modele.Trajet;

class TourneeTest {

	@Test
	void test() {
		//Tests sur constructeur de Tournee
		double longueur = 12.3;
		int livreur = 2;
		ArrayList<Pair<PointDeLivraison,Trajet>> listePointTrajet = new ArrayList<Pair<PointDeLivraison,Trajet>>();
		Tournee t = new Tournee (longueur, livreur, listePointTrajet);
		assertNotNull(t.getListePointTrajet());
		assertEquals(t.getLivreur(), 2);
		assertEquals(t.getLongueur(), 12.3);
		
	}

}
