package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Plan;
import modele.Trajet;
import modele.Troncon;

class PlanTest {

	
	private static Plan plan;
	private static ArrayList<Noeud> listeNoeuds;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		//HashMap<Long,Noeud> uneCollection = new HashMap<Long,Noeud>();
		
	
		
		
		Noeud Paris = new Noeud();
		Noeud Marseille = new Noeud();
		Noeud Rennes = new Noeud();
		Noeud Avignon = new Noeud();
		Troncon ParisMarseille= new Troncon(1,"Troncon 12",Paris,Marseille);
		Troncon ParisRennes = new Troncon(2,"Troncon 13",Paris,Rennes);
		Troncon ParisAvignon = new Troncon(3,"Troncon 14",Paris,Avignon);
		Troncon MarseilleParis = new Troncon(4,"Troncon 21",Marseille,Paris);
		Troncon MarseilleRennes = new Troncon(5,"Troncon 23",Marseille,Rennes);
		Troncon MarseilleAvignon = new Troncon(6,"Troncon 24",Marseille,Avignon);
		Troncon RennesParis = new Troncon(7,"Troncon 31",Rennes,Paris);
		Troncon RennesMarseille = new Troncon(8,"Troncon 32",Rennes,Marseille);
		Troncon RennesAvignon = new Troncon(9,"Troncon 34",Rennes,Avignon);
		Troncon AvignonParis = new Troncon(10,"Troncon 41",Avignon,Paris);
		Troncon AvignonMarseille = new Troncon(11,"Troncon 42",Avignon,Marseille);
		Troncon AvignonRennes = new Troncon(12,"Troncon 43",Avignon,Rennes);
		ArrayList<Troncon> tronconsParis = new ArrayList<>();
		ArrayList<Troncon> tronconsMarseille = new ArrayList<>();
		ArrayList<Troncon> tronconsRennes = new ArrayList<>();
		ArrayList<Troncon> tronconsAvignon = new ArrayList<>();
		tronconsParis.add(ParisMarseille);
		tronconsParis.add(ParisRennes);
		tronconsParis.add(ParisAvignon);
		tronconsMarseille.add(MarseilleParis);
		tronconsMarseille.add(MarseilleRennes);
		tronconsMarseille.add(MarseilleAvignon);
		tronconsRennes.add(RennesParis);
		tronconsRennes.add(RennesMarseille);
		tronconsRennes.add(RennesAvignon);
		tronconsAvignon.add(AvignonParis);
		tronconsAvignon.add(AvignonMarseille);
		tronconsAvignon.add(AvignonRennes);
		Paris.setTronconsPartants(tronconsParis);
		Marseille.setTronconsPartants(tronconsMarseille);
		Rennes.setTronconsPartants(tronconsRennes);
		Avignon.setTronconsPartants(tronconsAvignon);
		Paris.setId(new Long(1));
		Marseille.setId(new Long(2));
		Rennes.setId(new Long(3));
		Avignon.setId(new Long(4));
		HashMap<Long,Noeud>  HP = new HashMap<Long,Noeud>();
		HP.putIfAbsent(Paris.getId(),Paris);
		HP.putIfAbsent(Marseille.getId(),Marseille);
		HP.putIfAbsent(Rennes.getId(),Rennes);
		HP.putIfAbsent(Avignon.getId(),Avignon);
		listeNoeuds.add(Avignon);
		listeNoeuds.add(Marseille);
		listeNoeuds.add(Paris);
		listeNoeuds.add(Rennes);
		plan.setCollectionNoeuds(HP);
		
	}
	


	@Test
	void test() {
		System.out.println("Test ");
		ArrayList<Noeud> copieNoeuds =(ArrayList<Noeud>) listeNoeuds.clone();
		Collection<Trajet> recupTrajets = new ArrayList<Trajet>();
		for(Noeud n : listeNoeuds) {
			copieNoeuds.remove(n);
			recupTrajets = plan.calculerPlusCourtChemin(n,copieNoeuds);
			for(Trajet t : recupTrajets) {
				
				System.out.println(t.toString());
				
			}
			copieNoeuds.add(n);
			
		}
		
		fail("Not yet implemented");
	}

}
