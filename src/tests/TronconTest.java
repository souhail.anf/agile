package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.Troncon;

class TronconTest {

	@Test
	void test() {
		//Tests sur constructeur de Troncon
		double uneLongueur = 14.3;
		String unNom = "nom";
		Noeud uneOrigine  = new Noeud();
		Noeud uneDestination = new Noeud();
		Troncon t = new Troncon(uneLongueur, unNom, uneOrigine, uneDestination);
		assertNotNull(t.getNoeudDestination());
		assertNotNull(t.getNoeudOrigine());
		assertEquals(t.getLongueur(), 14.3);
	}

}
