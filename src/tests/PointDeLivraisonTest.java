package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import modele.Noeud;
import modele.PointDeLivraison;
import modele.Troncon;

class PointDeLivraisonTest {

	@Test
	void test() {
		//Tests sur constructeur complet de PointDeLivraison
		Long id = new Long(12);
		double latitude = 12.0;
		double longitude = 14.2;
		int duree = 16;
		ArrayList<Troncon> tronconsPartants = new ArrayList<Troncon>();
		PointDeLivraison pdl = new PointDeLivraison(id, latitude, longitude, tronconsPartants, duree);
		assertNotNull(pdl.getTronconsPartants());
		assertEquals(pdl.getDuree(),16);
		assertEquals(pdl.getLatitude(), 12.0);
		assertEquals(pdl.getLongitude(), 14.2);
		
		//Tests sur constructeur avec noeud de PointDeLivraison
		Noeud noeud = new Noeud(id,latitude,longitude,tronconsPartants);
		PointDeLivraison pdl2 = new	PointDeLivraison(noeud, duree);
		assertNotNull(pdl2.getTronconsPartants());
		assertEquals(pdl2.getDuree(),16);
		assertEquals(pdl2.getLatitude(), 12.0);
		assertEquals(pdl2.getLongitude(), 14.2);
	}

}
